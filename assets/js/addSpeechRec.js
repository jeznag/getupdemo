// based on https://github.com/GoogleChrome/webplatform-samples/blob/master/webspeechdemo/webspeechdemo.html
document.querySelector('#recordVoiceMessage').addEventListener('click', startTranscription);
let finalTranscript, started, recognition;

function startTranscription() {
  if (started) {
    recognition.stop();
  }
  if (('webkitSpeechRecognition' in window)) {
    recognition = new webkitSpeechRecognition();
    const emailBodyTextArea = document.querySelector('#user_email_body');
    recognition.continuous = true;
    recognition.interimResults = true;

    recognition.onstart = handleRecognitionStart;

    recognition.onerror = handleRecognitionError;

    recognition.onend = handleRecognitionEnd;

    recognition.onresult = handleRecognitionResult;

    recognition.start();

    started = true;
  }
}

function handleRecognitionStart() {
  console.log('start recognising');
}

function handleRecognitionError(e) {
  console.log(`ERROR`, e);
}

function handleRecognitionEnd() {
  console.log('finished recognising');
}

function handleRecognitionResult(event) {
  let interimTranscript = '';
  for (var i = event.resultIndex; i < event.results.length; ++i) {
    if (event.results[i].isFinal) {
      finalTranscript += event.results[i][0].transcript;
    } else {
      interimTranscript += event.results[i][0].transcript;
    }
  }
  console.log(interimTranscript);
  finalTranscript = capitalize(finalTranscript);
  emailBodyTextArea.innerHTML = linebreak(finalTranscript);
}

const firstChar = /\S/;
function capitalize(s) {
  return s.replace(firstChar, firstLetterOfSentence => firstLetterOfSentence.toUpperCase());
}

const twoLine = /\n\n/g;
const oneLine = /\n/g;
function linebreak(s) {
  return s.replace(twoLine, '<p></p>').replace(oneLine, '<br>');
}
