# getupdemo

Demo with speech recognition and sentiment analysis

[![Travis build status](http://img.shields.io/travis/jeznag/getupdemo.svg?style=flat)](https://travis-ci.org/jeznag/getupdemo)
[![Code Climate](https://codeclimate.com/github/jeznag/getupdemo/badges/gpa.svg)](https://codeclimate.com/github/jeznag/getupdemo)
[![Test Coverage](https://codeclimate.com/github/jeznag/getupdemo/badges/coverage.svg)](https://codeclimate.com/github/jeznag/getupdemo)
[![Dependency Status](https://david-dm.org/jeznag/getupdemo.svg)](https://david-dm.org/jeznag/getupdemo)
[![devDependency Status](https://david-dm.org/jeznag/getupdemo/dev-status.svg)](https://david-dm.org/jeznag/getupdemo#info=devDependencies)
