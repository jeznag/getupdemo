/**
 * getupdemo
 *
 * Copyright © 2016 . All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import activateSpeechRecognition from './addSpeechRecognition';
import addSentimentAnalysis from './analyseSentiment';
import saveToFirebase from './addToFirebase';

activateSpeechRecognition();
addSentimentAnalysis();

const sendButton = document.querySelector('#sendButton');
sendButton.addEventListener('click', saveToFirebase);
