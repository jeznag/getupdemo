import firebase from 'firebase';

const emailInput = document.querySelector('#user_email');
const postCodeInput = document.querySelector('#mp_postcode');
const emailSubjectInput = document.querySelector('#user_email_subject');
const emailBodyInput = document.querySelector('#user_email_body');

function initialiseFirebase() {
  const config = {
    apiKey: "AIzaSyD2Q2jI65cF8Mha3iem6GZQVmsgIgt-NKc",
    authDomain: "getup-demo.firebaseapp.com",
    databaseURL: "https://getup-demo.firebaseio.com",
    storageBucket: "",
    messagingSenderId: "426599411655"
  };
  firebase.initializeApp(config);
}

function generateFormSubmissionPayload() {
  debugger;
  return {
    campaign: 'marriage_equality',
    email: emailInput.value,
    postcode: postCodeInput.value,
    email_subject: emailSubjectInput.value,
    email_body: emailBodyInput.value
  };
}

export default function saveData() {
  console.log(generateFormSubmissionPayload());
  const database = firebase.database();
  const response = database.ref().child('submissions').push(generateFormSubmissionPayload());
  debugger;
}

initialiseFirebase();
