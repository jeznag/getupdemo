import Speech from 'speechjs';

const recordingMessageSpan = document.querySelector('#recording-message');
const emailBodyTextArea = document.querySelector('#user_email_body');
let started = false;
let recognizer;

function startTranscription() {
  if (started) {
    recognizer.stop();
    recordingMessageSpan.textContent = 'Ok stopping 😀';
    return;
  }

  recordingMessageSpan.textContent = 'Ok starting recording';

  if (!recognizer) {
    recognizer = new Speech({
      // all boolean options default to false
      debugging: true, // will console.log all results
      continuous: true, // will not stop after one sentence
      interimResults: true, // trigger events on iterim results
      autoRestart: true // restart periodically in case of network error
    });
  }

  // simply listen to events
  // chainable API
  recognizer
      .on('start', () => {
        recordingMessageSpan.textContent = 'Recording started';
        console.log('started');
      })
      .on('end', () => {
        console.log('ended');
      })
      .on('error', event => {
        recordingMessageSpan.textContent = 'Oops there was a problem recording';
        console.log(event.error);
      })
      .on('interimResult', msg => {
        recordingMessageSpan.textContent = `Here's what I think I heard`;
        emailBodyTextArea.innerHTML += msg;
      })
      .on('finalResult', msg => {
        recordingMessageSpan.textContent = `Here's what I got. Is it right?`;
        emailBodyTextArea.innerHTML = msg;
      })
      .start();

  started = true;
}

export default function activateSpeechRecognition() {
  document.querySelector('#recordVoiceMessage').addEventListener('click', startTranscription);
}
