import empath from 'empath-sentiment-analysis';

const emailBodyTextArea = document.querySelector('#user_email_body');
const sentimentFeedbackDiv = document.querySelector('#sentiment-feedback');
const attributesOfInterest = ['marriage', 'senate'];

function checkInput() {
  const currentEmailBody = emailBodyTextArea.value;
  console.log(currentEmailBody);
  if (currentEmailBody.length < 150) {
    return;
  }

  const firstTwoSentences = currentEmailBody.substring(0, 100);
  const restOfEmail = currentEmailBody.substring(100);
  const firstTwoSentencesResult = empath.analyseSentiment(firstTwoSentences, attributesOfInterest);
  const restOfEmailResult = empath.analyseSentiment(restOfEmail, attributesOfInterest);

  showResults(firstTwoSentencesResult, restOfEmailResult);
}

function showResults(firstTwoSentencesResult, restOfEmailResult) {
  console.log(firstTwoSentencesResult, restOfEmailResult);
  const feedbackOnFirstTwoSentences = firstTwoSentencesResult.score >= 0 ?
    `<span class="feedback good">Nice one! You've started off the email politely which is always good. The politeness score was ${firstTwoSentencesResult.score}.</span>` :
    `<span class="feedback bad">Consider softening up the first few sentences of the email. The politeness score was ${firstTwoSentencesResult.score}. We find pollies are more responsive if you start politely.</span>`
  const feedbackOnRestOfEmail = restOfEmailResult.score < 0 ?
    `<span class="feedback good">The rest of the email is very strong. The politeness score was ${restOfEmailResult.score}. That's excellent. Don't hold anything back!</span>` :
    `<span class="feedback bad">How about you make the rest of the email punchier? The politeness score was ${restOfEmailResult.score} which is a bit too polite! It's important to tell them exactly how you feel about the issue.</span>`;

  sentimentFeedbackDiv.innerHTML = `${feedbackOnFirstTwoSentences}${feedbackOnRestOfEmail}`;
}

export default function addSentimentAnalysis() {
  emailBodyTextArea.addEventListener('change', checkInput);
  emailBodyTextArea.addEventListener('keyup', checkInput);
}
