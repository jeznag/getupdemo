import getupdemo from '../../src/getupdemo';

describe('getupdemo', () => {
  describe('Greet function', () => {
    beforeEach(() => {
      spy(getupdemo, 'greet');
      getupdemo.greet();
    });

    it('should have been run once', () => {
      expect(getupdemo.greet).to.have.been.calledOnce;
    });

    it('should have always returned hello', () => {
      expect(getupdemo.greet).to.have.always.returned('hello');
    });
  });
});
